import sys
from functools import reduce

from flask import Flask
from flask.ext.pymongo import PyMongo

app = Flask(__name__, static_url_path='/static')
mongo = PyMongo(app)

@app.route("/")
def hello():
    stuff = mongo.db.stuff.find()
    exp = """
    <pre>

> db.stuff.insert({ "name": "Moshe" })
WriteResult({ "nInserted" : 1 })
> db.stuff.insert({ "name": "David" })
WriteResult({ "nInserted" : 1 })
> db.stuff.insert({ "name": "Orr" })
WriteResult({ "nInserted" : 1 })
> db.stuff.insert({ "name": "Tamir" })
WriteResult({ "nInserted" : 1 })
> db.stuff.insert({ "name": "Ben" })
WriteResult({ "nInserted" : 1 })
> db.stuff.insert({ "name": "Arye" })
WriteResult({ "nInserted" : 1 })
> </pre>"""

    ls = reduce(lambda x, y: x + y, list(map(lambda x: "<li>" + x['name'] + "</li>", stuff)))
    return "<h1>Welcome Jovial fellows!</h1><a href=\"static/index.html\">Go see more...</a>" + exp + "<ul>" + ls + "</ul>"

@app.route("/hello/<name>") 
def hi(name): 
    return "<h1>Hello, " + name + "?</h1>" 

@app.route("/test") 
def test(): 
    return '{ "name": "Moshe" }'

#url_for('static')

if __name__ == "__main__":
    isDebug = len(sys.argv) < 2
    runPort = 5000 if len(sys.argv) < 2 or len(sys.argv[1]) == 0 else int(sys.argv[1])
    app.run(host='0.0.0.0', port=runPort, debug=True)
